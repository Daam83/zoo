package pl.codemasters.zoo;

/**
 * Created by student on 08.06.17.
 */
public interface Herbivorous {
    void eatPlant();
    
}
