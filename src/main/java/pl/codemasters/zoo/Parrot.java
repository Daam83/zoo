package pl.codemasters.zoo;

/**
 * Created by student on 06.06.17.
 */
public class Parrot extends Bird implements Herbivorous {
    public void screech() {

        System.out.println("Papuga " + getAnimalName() + " skrzeczy");
    }

    public void eat() {

        System.out.println("Papuga " + getAnimalName() + " je");
    }

    @Override
    public void eatPlant() {
        eat();

    }
}
