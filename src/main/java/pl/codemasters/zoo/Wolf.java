package pl.codemasters.zoo;

/**
 * Created by student on 06.06.17.
 */
public class Wolf extends Mammal implements Carnivorous {
    public void howl() {
        System.out.println("Wilk " + getAnimalName() + " wyje");
    }
    @Override
    public void eat() {
        System.out.println("Wilk " + getAnimalName() + " je");
    }

    @Override
    public void eatMeat() {

        eat();
        System.out.println("je mięso");

    }
}
