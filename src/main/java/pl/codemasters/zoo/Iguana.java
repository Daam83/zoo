package pl.codemasters.zoo;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.List;

/**
 * Created by student on 06.06.17.
 */
public class Iguana extends Lizard implements Herbivorous {
    public void hiss() {
        System.out.println("Iguana " + getAnimalName() + " syczy");
    }

    @Override
    public void eat() {
        System.out.println("Iguana " + getAnimalName() + " je");
    }

    @Override
    public void eatPlant() {
        eat();

    }
}
