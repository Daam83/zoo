package pl.codemasters.zoo;

/**
 * Created by student on 06.06.17.
 */
public abstract class Animal {
    private String animalName;
    private int age;

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
    //InteliiJ set error if abstract metod eat() have a body. "the abstract class can't have body
}
